<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', function () {
    return view('index');
})->name("home");

Route::get('/', 'Controller@index');
Route::get('/home', 'Controller@index')->name("home");

Route::get('/streamer/Trimidzz', function() {
    return view('streamers/trimidzz');
})->name('trimidzz');

Route::get('/streamer/4rch3r', function() {
    return view('streamers/archer');
})->name('archer');

Route::get('/competition', 'CompetitionController@index');
Route::get('/competition/launch', 'CompetitionController@launch');
Route::get('/competition/result/{id}/{result}', 'CompetitionController@result');
Route::get('/competition/{id}', 'CompetitionController@detail');
Route::get('/competition/game/{id}', 'CompetitionController@index');
Route::get('/competition/force_winner/{id}/{user}', 'CompetitionController@forceWinner');

Route::get('/streamer', 'StreamerController@index');

Route::get('contact', 'ContactUSController@contactUS');
Route::post('contact', ['as'=>'contactus.store','uses'=>'ContactUSController@contactSaveData']);

Route::get('/competition/inscription/{id}', 'CompetitionController@inscription');
Route::get('/competition/desinscription/{id}', 'CompetitionController@desinscription');

Route::get('/account/historic', 'WalletHistoricController@historic');
Route::get('/account', 'UserController@account');
Route::get('/account/profile', 'ProfileController@edit');
Route::post('/account/update', 'ProfileController@update');

Route::post('message', ['as'=>'messages.store','uses'=>'MessageController@messageSaveData']);
Route::get('/messages/match/{id}', 'MessageController@getByMatch');

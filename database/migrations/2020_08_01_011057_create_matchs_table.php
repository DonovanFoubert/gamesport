<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id_1')->unsigned()->nullable();//Génération des matchs;
            $table->bigInteger('user_id_2')->unsigned()->nullable();
            $table->bigInteger('tournament_id')->unsigned();
            $table->bigInteger('next_match')->unsigned()->nullable();
            $table->bigInteger('next_position')->unsigned()->nullable();
            $table->bigInteger('winner')->unsigned()->nullable();
            $table->boolean('result_1')->unsigned()->nullable();
            $table->boolean('result_2')->unsigned()->nullable();
            $table->integer('round')->unsigned();
            $table->foreign('user_id_1')->references('id')->on('users');
            $table->foreign('user_id_2')->references('id')->on('users');
            $table->foreign('tournament_id')->references('id')->on('tournaments');
            $table->foreign('next_match')->references('id')->on('matchs');
            $table->foreign('winner')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matchs', function(Blueprint $table) {
            $table->dropForeign('matchs_user_id_1_foreign');
            $table->dropForeign('matchs_user_id_2_foreign');
            $table->dropForeign('matchs_tournament_id_foreign');
            $table->dropForeign('matchs_next_match_foreign');
            $table->dropForeign('matchs_winner_foreign');
        });

        Schema::dropIfExists('matchs');
    }
}

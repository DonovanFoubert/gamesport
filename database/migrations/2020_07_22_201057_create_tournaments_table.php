<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->id();
            $table->Date('date');
            $table->Time('start');
            $table->Time('end');
            $table->float('inscription');
            $table->float('cashprize')->nullable();
            $table->string('lot',255)->nullable();
            $table->boolean('live');
            $table->bigInteger('console_id')->unsigned();
            $table->foreign('console_id')->references('id')->on('consoles');
            $table->bigInteger('game_id')->unsigned();
            $table->foreign('game_id')->references('id')->on('games');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tournaments', function(Blueprint $table) {
            $table->dropForeign('tournaments_console_id_foreign');
            $table->dropForeign('tournaments_game_id_foreign');
        });

        Schema::dropIfExists('tournaments');
    }
}

<?php

use Illuminate\Database\Seeder;

class MatchSeeder extends Seeder {
    public function run()
    {
        DB::table('matchs')->delete();

        DB::table('matchs')->insert([
            'id' => 7,
            'user_id_1' => 1,
            'user_id_2' => 5,
            'tournament_id' => '1',
            'next_match' => null,
            'winner' => 1,
            'round' => 1,
            'next_position' => null,
            'result_1' => null,
            'result_2' => null
        ]);

        DB::table('matchs')->insert([
            'id' => 6,
            'user_id_1' => 5,
            'user_id_2' => 9,
            'tournament_id' => '1',
            'next_match' => 7,
            'winner' => 5,
            'round' => 2,
            'next_position' => 1,
            'result_1' => null,
            'result_2' => null
        ]);

        DB::table('matchs')->insert([
            'id' => 5,
            'user_id_1' => 1,
            'user_id_2' => 3,
            'tournament_id' => '1',
            'next_match' => 7,
            'winner' => 1,
            'round' => 2,
            'next_position' => 2,
            'result_1' => null,
            'result_2' => null
        ]);

        DB::table('matchs')->insert([
            'id' => 4,
            'user_id_1' => 5,
            'user_id_2' => 6,
            'tournament_id' => '1',
            'next_match' => 6,
            'winner' => 5,
            'round' => 4,
            'next_position' => 1,
            'result_1' => null,
            'result_2' => null
        ]);

        DB::table('matchs')->insert([
            'id' => 3,
            'user_id_1' => 3,
            'user_id_2' => 4,
            'tournament_id' => '1',
            'next_match' => 5,
            'winner' => 3,
            'round' => 4,
            'next_position' => 1,
            'result_1' => null,
            'result_2' => null
        ]);


        DB::table('matchs')->insert([
            'id' => 2,
            'user_id_1' => 1,
            'user_id_2' => 2,
            'tournament_id' => 1,
            'next_match' => 5,
            'winner' => 1,
            'round' => 4,
            'next_position' => 2,
            'result_1' => null,
            'result_2' => null
        ]);

        DB::table('matchs')->insert([
            'id' => 1,
            'user_id_1' => 9,
            'user_id_2' => null,
            'tournament_id' => '1',
            'next_match' => 6,
            'winner' => 9,
            'round' => 4,
            'next_position' => 2,
            'result_1' => null,
            'result_2' => null
        ]);
    }
}

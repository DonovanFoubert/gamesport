<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(ConsoleSeeder::class);
        $this->call(GameSeeder::class);
        $this->call(GamerSeeder::class);
        $this->call(TournamentSeeder::class);
        $this->call(TournamentUserSeeder::class);
        $this->call(WalletHistoricSeeder::class);
        $this->call(MatchSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class GameSeeder extends Seeder {
    public function run()
    {
        DB::table('games')->delete();

        DB::table('games')->insert([
            'title' => 'FIFA 20',
            'image' => '/img/games/fifa-20.jpg',
            'genre' => 'Sport'
        ]);

        DB::table('games')->insert([
            'title' => 'Call of Duty',
            'image' => '/img/games/callof.jpg',
            'genre' => 'FPS'
        ]);

        DB::table('games')->insert([
            'title' => 'Rainbow Six Siege',
            'image' => '/img/games/rainbow.jpg',
            'genre' => 'FPS'
        ]);
    }
}

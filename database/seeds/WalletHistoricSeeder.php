<?php

use Illuminate\Database\Seeder;

class WalletHistoricSeeder extends Seeder {
    public function run()
    {
        DB::table('wallet_historic')->delete();

        DB::table('wallet_historic')->insert([
            'user_id' => '1',
            'type' => 'debit',
            'action' => 'Achat de crédit',
            'amount' => 25,
            'wallet' => 25,
            'created_at' => '2020-07-01 17:00:00'
        ]);

        DB::table('wallet_historic')->insert([
            'user_id' => '1',
            'type' => 'credit',
            'action' => 'Inscription tournoi 1',
            'amount' => 1,
            'wallet' => 24,
            'created_at' => '2020-07-01 21:00:00'
        ]);

        DB::table('wallet_historic')->insert([
            'user_id' => '1',
            'type' => 'debit',
            'action' => 'Annulation tournoi 1',
            'amount' => 1,
            'wallet' => 25,
            'created_at' => '2020-07-01 21:05:00'
        ]);

        DB::table('wallet_historic')->insert([
            'user_id' => '1',
            'type' => 'credit',
            'action' => 'Inscription tournoi 2',
            'amount' => 1,
            'wallet' => 24,
            'created_at' => '2020-09-01 20:00:00'
        ]);

    }
}

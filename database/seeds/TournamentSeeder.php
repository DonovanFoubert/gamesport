<?php

use Illuminate\Database\Seeder;

class TournamentSeeder extends Seeder {
    public function run()
    {
        DB::table('tournaments')->delete();

        DB::table('tournaments')->insert([
            'date' => "2020-09-01",
            'start' => '18:00:00',
            'end' => '23:00:00',
            'console_id' => '1',
            'game_id' => '1',
            'inscription' => 10,
            'cashprize' => 200,
            'live' => 0
        ]);

        DB::table('tournaments')->insert([
            'date' => "2020-10-01",
            'start' => '18:00:00',
            'end' => '23:00:00',
            'console_id' => '2',
            'game_id' => '3',
            'inscription' => 2,
            'lot' => "T-Shirt Winnie L'Ourson",
            'live' => 0

        ]);

        DB::table('tournaments')->insert([
            'date' => "2020-11-01",
            'start' => '18:00:00',
            'end' => '23:00:00',
            'console_id' => '1',
            'game_id' => '2',
            'inscription' => 1,
            'cashprize' => 20,
            'live' => 0
        ]);
    }
}

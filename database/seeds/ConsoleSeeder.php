<?php

use Illuminate\Database\Seeder;

class ConsoleSeeder extends Seeder {
    public function run()
    {
        DB::table('consoles')->delete();

        DB::table('consoles')->insert([
            'console' => 'PlayStation 4',
            'logo' => '/img/consoles/playstation-4.png'
        ]);

        DB::table('consoles')->insert([
            'console' => 'Xbox One',
            'logo' => '/img/consoles/xbox-one.png'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class GamerSeeder extends Seeder {
    public function run()
    {
        DB::table('gamers')->delete();

        DB::table('gamers')->insert([
            'pseudo' => 'Trimidzz',
            'firstname' => 'Dimitri',
            'name' => 'Deghilage',
            'picture' => '/img/gamers/dimitri.jpg',
            'function' => 'Streamer & Compétiteur',
            'birthday' => '1995-05-28'
        ]);

        DB::table('gamers')->insert([
            'pseudo' => '4rch3r',
            'firstname' => 'Benjamin',
            'name' => 'Piquet',
            'picture' => '/img/gamers/benjamin.jpg',
            'function' => 'Streamer & Compétiteur',
            'birthday' => '2009-04-01'
        ]);
    }
}

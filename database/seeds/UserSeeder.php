<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name' => 'Donovan',
            'ps_id' => 'TwentyFree23',
            'xbox_id' => 'DonoT2J',
            'email' => 'donovan@prograticweb.be',
            'password' => bcrypt('Mdp1234*'),
            'wallet' => 23
        ]);

        for($i = 0; $i < 10; ++$i)
        {
            DB::table('users')->insert([
                'name' => 'Nom' . $i,
                'ps_id' => 'ps' . $i,
                'xbox_id' => 'xbox' . $i,
                'email' => 'email' . $i . '@blop.fr',
                'password' => bcrypt('password' . $i),
                'wallet' => $i
            ]);
        }

        DB::table('users')->insert([
            'name' => 'Sandy Foubert',
            'ps_id' => 'SanZeven',
            'xbox_id' => 'SanZeven',
            'email' => 'sandy_foubert@hotmail.fr',
            //'password' => "$2y$10$x5MXBDTt6tpDv1FngzwDieJ92IT/nMnGVPscEyx8FGW3EtFIm4Wee",
            'password' => bcrypt("test"),
            'wallet' => 11
        ]);
    }
}

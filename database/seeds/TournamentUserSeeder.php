<?php

use Illuminate\Database\Seeder;

class TournamentUserSeeder extends Seeder {
    public function run()
    {
        DB::table('tournament_user')->delete();

        DB::table('tournament_user')->insert([
            'tournament_id' => '1',
            'user_id' => '1'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '1',
            'user_id' => '2'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '1',
            'user_id' => '3'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '1',
            'user_id' => '4'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '2',
            'user_id' => '4'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '2',
            'user_id' => '5'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '2',
            'user_id' => '6'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '3',
            'user_id' => '7'
        ]);

        DB::table('tournament_user')->insert([
            'tournament_id' => '3',
            'user_id' => '8'
        ]);

    }
}

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Le champ :attribute doit être accepté.',
    'active_url' => 'Le champ :attribute est une URL invalide.',
    'after' => 'Le champ :attribute doit être une date suppérieur à :date.',
    'after_or_equal' => 'Le champ :attribute doit être une date suppérieur ou égale à :date.',
    'alpha' => 'Le champ :attribute doit uniquement être composé de lettres.',
    'alpha_dash' => 'Le champ :attribute peut uniquement contenir des lettres, chiffres, tirets and underscores.',
    'alpha_num' => 'Le champ :attribute peut uniquement être composé de chiffres et de lettres.',
    'array' => 'Le champ :attribute doit être un tableau.',
    'before' => 'Le champ :attribute doit être une date inférieure à :date.',
    'before_or_equal' => 'Le champ :attribute doit être une date inférieur ou égale à :date.',
    'between' => [
        'numeric' => 'Le champ :attribute doit être entre :min et :max.',
        'file' => 'Le champ :attribute doit avoir entre :min et :max kilobytes.',
        'string' => 'Le champ :attribute doit être composé de :min à :max characters.',
        'array' => 'Le champ :attribute doit avoir entre :min et :max objets.',
    ],
    'boolean' => 'Le champ :attribute doit être un booléen.',
    'confirmed' => 'Le champ :attribute confirmation ne correspond pas.',
    'date' => "Le champ :attribute n'est pas une date valide.",
    'date_equals' => 'Le champ :attribute doit être une date égale à :date.',
    'date_format' => 'Le champ :attribute ne correspond pas au format :format.',
    'different' => 'Le champ :attribute et :other doivent être différents.',
    'digits' => 'Le champ :attribute doit comprendre :digits nombres.',
    'digits_between' => 'Le champ :attribute doit être un chiffre entre :min et :max nombres.',
    'dimensions' => "Le champ :attribute à une dimension d'image invalide.",
    'distinct' => 'Le champ :attribute a une valeur dupliquée.',
    'email' => 'Le champ :attribute doit être une adresse mail valide.',
    'ends_with' => 'Le champ :attribute doit se terminer par: :values.',
    'exists' => 'Le champ choisi :attribute est invalide.',
    'file' => 'Le champ :attribute doit être un fichier.',
    'filled' => 'Le champ :attribute doit être défini.',
    'gt' => [
        'numeric' => 'Le champ :attribute doit être plus grand que :value.',
        'file' => 'Le champ :attribute doit avoir une taille suppérieur à :value kilobytes.',
        'string' => 'Le champ :attribute doit être plus longs que :value caractères.',
        'array' => 'Le champ :attribute doit contenur plus de :value objets.',
    ],
    'gte' => [
        'numeric' => 'Le champ :attribute doit être suppérieur ou égale à :value.',
        'file' => 'Le champ :attribute doit avoir une taille suppérieur ou égale à :value kilobytes.',
        'string' => 'Le champ :attribute doit être plus longs ou égal à :value characters.',
        'array' => 'Le champ :attribute doit avoir :value objets au minimum.',
    ],
    'image' => 'Le champ :attribute doit être une image.',
    'in' => 'Le champs choisi :attribute est invalide.',
    'in_array' => "Le champ :attribute n'existe pas dans :other.",
    'integer' => 'Le champ :attribute doit être un numérique.',
    'ip' => 'Le champ :attribute doit être une adresse IP valide.',
    'ipv4' => 'Le champ :attribute doit être une adresse IPv4 valide.',
    'ipv6' => 'Le champ :attribute doit être une adresse IPv6 valide.',
    'json' => 'Le champ :attribute doit être une chaîne JSON valide.',
    'lt' => [
        'numeric' => 'Le champ :attribute doit être plus petit que :value.',
        'file' => 'Le champ :attribute doit peser moins de :value kilobytes.',
        'string' => 'Le champ :attribute doit contenir moins de :value caractères.',
        'array' => 'Le champ :attribute doit avoir moins de :value objets.',
    ],
    'lte' => [
        'numeric' => 'Le champ :attribute ne peut pas dépasser :value.',
        'file' => 'Le champ :attribute ne peut pas peser plus de :value kilobytes.',
        'string' => 'Le champ :attribute ne peut pas contenir plus de :value caractères.',
        'array' => 'Le champ :attribute ne peut pas contenir plus de :value objets.',
    ],
    'max' => [
        'numeric' => 'Le champ :attribute ne peut pas dépasser :max.',
        'file' => 'Le champ :attribute ne doit pas peser plus de :max kilobytes.',
        'string' => 'Le champ :attribute ne doit pas être suppérieur à :max caractères.',
        'array' => 'Le champ :attribute ne peut pas contenir plus de :max objets.',
    ],
    'mimes' => 'Le champ :attribute doit être un fichier du type: :values.',
    'mimetypes' => 'Le champ :attribute doit être un fichier de type: :values.',
    'min' => [
        'numeric' => 'Le champ :attribute doit être au minimum :min.',
        'file' => 'Le  champ :attribute doit peser au moins :min kilobytes.',
        'string' => 'Le champ :attribute doit contenir au moins :min caractères.',
        'array' => 'Le champ :attribute doit contenir au moins :min objets.',
    ],
    'not_in' => 'Le champ choisi :attribute est invalide.',
    'not_regex' => 'Le format du champ :attribute est invalide.',
    'numeric' => 'Le champ :attribute doit être un nombre.',
    'password' => 'Le mot de passe est incorrect.',
    'present' => 'Le champ :attribute doit être présent.',
    'regex' => 'Le format du champ :attribute est invalide.',
    'required' => 'Le champ :attribute est requis.',
    'required_if' => 'Le champ :attribute est requis quand :other vaut :value.',
    'required_unless' => 'Le champ :attribute est requis quand :other est dans :values.',
    'required_with' => 'Le champ :attribute est requis quand :values est présent.',
    'required_with_all' => 'Le champ :attribute est requis quand :values est présent.',
    'required_without' => "Le champ :attribute est requis quand :values n'est pas présent.",
    'required_without_all' => "Le champ :attribute est requis si aucun :values n'est présent.",
    'same' => 'Les champs :attribute et :other doivent correspondre.',
    'size' => [
        'numeric' => 'Le champ :attribute doit avoir une taille de :size.',
        'file' => 'Le champ :attribute doit peser :size kilobytes.',
        'string' => 'Le champ :attribute doit être composé de :size caractères.',
        'array' => 'Le champ :attribute doit contenur :size objets.',
    ],
    'starts_with' => 'Le champ :attribute doit débuter par: :values.',
    'string' => 'Le champ :attribute doit être une chaine de caractères.',
    'timezone' => 'Le champ :attribute doit être une zone valide.',
    'unique' => 'Le champ :attribute est déjà pris.',
    'uploaded' => "L'upload de :attribute a échoué.",
    'url' => 'Le format du champ :attribute est invalide.',
    'uuid' => 'Le champ :attribute doit être un UUID valide.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Votre mot de passe a été réinitialisé!',
    'sent' => 'Nous vous avons envoyé votre lien de réinitialisation par mail!',
    'throttled' => 'Merci de patienter avant de réessayer.',
    'token' => 'Ce jeton de réinitialisation du mot de passe est invalide.',
    'user' => "Nous ne parvenons pas à retrouver un utilisateur avec cette adresse mail.",

];

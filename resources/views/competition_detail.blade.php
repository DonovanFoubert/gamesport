@extends('layouts.app')

@section('autoload')
@endsection

@section('content')

    <div class="team_area team_bg_1 overlay2">
        <div class="container">

            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif

            <div class="row bg-dark rounded-lg">
                @if(isset($adminMatchs) && count($adminMatchs) > 0)
                    @foreach($adminMatchs as $match)
                        @if(!isset($myMatch) || $match->id != $myMatch->id)
                            <div  class="row p-3 col-7">
                                @if($competition->console->id == 1)
                                    @php $user1 = ((isset($match->user1))?$match->user1->ps_id:"En attente"); @endphp
                                    @php $user2 = ((isset($match->user2))?$match->user2->ps_id:"En attente"); @endphp
                                @elseif($competition->console->id == 2)
                                    @php $user1 = ((isset($match->user1))?$match->user1->xbox_id:"En attente"); @endphp
                                    @php $user2 = ((isset($match->user2))?$match->user2->xbox_id:"En attente"); @endphp
                                @else
                                    @php $user1 = ((isset($match->user1))?$match->user1->warzone_id:"En attente"); @endphp
                                    @php $user2 = ((isset($match->user2))?$match->user2->warzone_id:"En attente"); @endphp
                                @endif
                                <h2 class="text-success col-lg-12">Admin - Les matchs en cours</h2>
                                <p class="col-xl-12">{{ $user1 . " - " . $user2}}
                                    @if(isset($match->user1) && isset($match->user2) && \App\Http\Middleware\IsAdmin::check())
                                        <a onclick="return confirm('Voulez vous vraiment forcer la victoire de {{ $user1 }}?')" href="{{ url("/competition/force_winner/".$match->id."/1") }}" class="text-white btn btn-success rounded ml-5"><i class="fas fa-thumbs-o-up"></i>Qualifier {{ $user1 }}</a>
                                        <a onclick="return confirm('Voulez vous vraiment forcer la victoire de {{ $user2 }}?')" href="{{ url("/competition/force_winner/".$match->id."/2") }}" class="text-white btn btn-danger rounded ml-5"><i class="fsa fa-thumbs-o-down"></i>Qualifier {{ $user2 }}</a>
                                    @endif
                                </p>
                            </div>
                            <div  class="row p-3 col-5">
                                <h3 class="text-white">Messages</h3>
                                <div id="messages{{ $match->id }}" style="max-height: 200px; overflow-scrolling: auto; overflow: auto; width: 100%;"></div>
                                <form method="post" action="{{ route('messages.store') }}" class="col-12">
                                    {{ csrf_field() }}
                                    <input type="text" name="message" class="col-8">
                                    <input type="hidden" name="match_id" value="{{ $match->id }}">
                                    <input type="submit" class="col-3">
                                </form>
                            </div>
                        @endif
                    @endforeach
                @endif

                @if(isset($myMatch) && isset($myMatch->user1) && isset($myMatch->user2))
                    <div  class="row p-3 col-7">
                        @if($competition->console->id == 1)
                            @php $user1 = ((isset($myMatch->user1))?$myMatch->user1->ps_id:"En attente"); @endphp
                            @php $user2 = ((isset($myMatch->user2))?$myMatch->user2->ps_id:"En attente"); @endphp
                        @elseif($competition->console->id == 2)
                            @php $user1 = ((isset($myMatch->user1))?$myMatch->user1->xbox_id:"En attente"); @endphp
                            @php $user2 = ((isset($myMatch->user2))?$myMatch->user2->xbox_id:"En attente"); @endphp
                        @else
                            @php $user1 = ((isset($myMatch->user1))?$myMatch->user1->warzone_id:"En attente"); @endphp
                            @php $user2 = ((isset($myMatch->user2))?$myMatch->user2->warzone_id:"En attente"); @endphp
                        @endif
                            @php $myPlace = ($myMatch->user1->id == \Illuminate\Support\Facades\Auth::id()) ? 1 : 2; @endphp
                            @php $otherPlace = ($myPlace == 1) ? 2 : 1; @endphp
                        <h2 class="text-danger col-lg-12">Mon match en cours</h2>
                        <p class="col-xl-12">{{ $user1 . " - " . $user2}} @if(isset($myMatch->user1) && isset($myMatch->user2)) <a href="{{ url("/competition/result/".$myMatch->id."/1") }}" class="text-white btn btn-success rounded ml-5"><i class="fas fa-thumbs-o-up"></i>Gagné!!!</a> <a href="{{ url("/competition/result/".$myMatch->id."/0") }}" class="text-white btn btn-danger rounded ml-5"><i class="fsa fa-thumbs-o-down"></i>Perdu...</a> @endif </p>
                        @if(isset($myMatch->user1) && isset($myMatch->user2) && \App\Http\Middleware\IsAdmin::check())
                            <p class="col-xl-12">Je suis admin
                                <a onclick="return confirm('Voulez vous vraiment forcer votre victoire?')" href="{{ url("/competition/force_winner/".$myMatch->id."/".$myPlace) }}" class="text-white btn btn-success rounded ml-5"><i class="fas fa-thumbs-o-up"></i>Forcer ma victoire</a>
                                <a  onclick="return confirm('Voulez vous vraiment forcer votre défaite?')"href="{{ url("/competition/force_winner/".$myMatch->id."/".$otherPlace) }}" class="text-white btn btn-danger rounded ml-5"><i class="fsa fa-thumbs-o-down"></i>Forcer ma défaite</a
                            </p>
                            @endif
                    </div>
                    <div  class="row p-3 col-5">
                        <h3 class="text-white">Messages</h3>
                        <div id="messages{{$myMatch->id}}" style="max-height: 200px; overflow-scrolling: auto; overflow: auto; width: 100%;"></div>
                        <form method="post" action="{{ route('messages.store') }}" class="col-12">
                            {{ csrf_field() }}
                            <input type="text" name="message" class="col-8">
                            <input type="hidden" name="match_id" value="{{ $myMatch->id }}">
                            <input type="submit" class="col-3">
                        </form>
                    </div>
                @endif

                <div class="col-xl-3 p-3">
                    <img src="{{ asset($competition->game->image) }}" height="300px">
                </div>

                <div class="col-xl-4 p-3">
                    <h4 class="text-white">Date: {{ date("d/m/Y", strtotime($competition->date)) }}</h4>
                    <p>De {{ substr($competition->start,0,5)." à ".substr($competition->end,0,5) }}</p>
                    <p>Plateforme: {{ $competition->console->console }}</p>
                    @if(isset($competition->game->rule))<p>Règlement: <a target="_blank" href="{{ url($competition->game->rule) }}">Consulter</a></p>@endif
                    @if(isset($competition->cashprize) && !empty($competition->cashprize))<p>Cashprize (gains répartis) : {{ $competition->cashprize }}€ <br> Répartition: 1er: {{ round((($competition->cashprize*65)/100),2) }} € - 2ème: {{ round((($competition->cashprize*35)/100),2) }} €</p>
                    @else<p>Lot pour le vainqueur : {{ $competition->lot }}</p>
                    @endif
                    <p>Inscription (frais d'entrée) : {{ $competition->inscription }}€</p><br>
                    @if(auth()->check() && $competition->live === 0)
                        @if($suscribed)
                            <span class="text-white">Je suis inscrit!</span> <a class="text-danger" style="cursor: pointer;" href="{{ url("/competition/desinscription/".$competition->id) }}">Me désinscrire</a>
                        @else
                            <a href="{{ url("/competition/inscription/".$competition->id) }}" class="btn-success rounded-lg p-2">Participer</a>
                        @endif
                    @elseif($competition->live === 0)
                        <p><a href="{{ url("/login") }}?prvUrl={{ request()->fullUrl() }}">Connectez-vous</a> ou <a href="{{ url("/register") }}?prvUrl={{ request()->fullUrl() }}">Inscrivez-vous</a> pour y participer!</p>
                    @endif
                </div>
                <div class="col-xl-2 p-3"></div>
                <div class="col-xl-3 p-3">
                    <h3 class="text-white">Participants ({{ count($competition->users) }})</h3>
                    <p style="max-height: 200px; overflow: scroll;">
                        @foreach($competition->users as $user)
                            @if($competition->console->id == 1)
                                @php $pseudo = $user->ps_id; @endphp
                            @elseif($competition->console->id == 2)
                                @php $pseudo = $user->xbox_id; @endphp
                            @else
                                @php $pseudo = $user->warzone_id; @endphp
                            @endif
                            {{ $pseudo }}<br>
                        @endforeach
                    </p>
                </div>
                    <br><br>
                    <div class="row p-3 col-12">
                        <h2 class="text-white col-lg-12">Déroulement</h2>
                        <p class="p-3">
                            @if($competition->console->id != 3)
                                1. Les premiers matchs sont générés à l'heure exacte du début du tournoi ;<br><br>
                                2. Vous contactez l'adversaire qui vous a été attribué afin de lancer le match au plus vite ;<br><br>
                                3. En cas de non réponse de votre adversaire dans les dix minutes, envoyez nous la preuve par Messenger ;<br><br>
                                4. Une fois le match terminé les deux joueurs valident le résultat sur la plateforme ;<br><br>
                                5. Si vous avez perdu, pas de chance, revenez vite disputer une autre compétition... ;<br><br>
                                6. Si vous avez gagné, un nouvel adversaire vous sera communiqué en fonction des résultats des autres matchs et ce jusqu'à la victoire finale!<br><br>
                            @else
                                Les invitations à la partie privée sont envoyées environ 15 minutes avant le début de la partie; <br><br>
                            @endif
                            Bonne chance à tous !!!
                        </p>
                    </div>
                    <br><br>
                    <div class="row p-3 col-12">
                        @if($competition->console->id != 3)
                            <h2 class="text-white col-lg-12">Matchs</h2>

                            @foreach($rounds as $key => $round)
                                <div class="col-xl-3 text-center">
                                    <h2 class="text-white bold">{{ ($key != 1)?"1/".$key:"Finale" }}</h2>
                                </div>
                            @endforeach
                            <div class="col-xl-12"></div>
                            @foreach($rounds as $key => $round)
                                <div class="col-xl-3">
                                    @foreach($round as $match)
                                        <div style="height: {{ (100/$key) }}%; display: flex; align-items: center;">
                                            <div style="border: 1px solid black; width: 100%; text-align: center; margin-bottom: 10px;" class="rounded">
                                                @if($competition->console->id == 1)
                                                    <p class="rounded-top {{ (isset($match->user1) && $match->user1["id"] == $match->winner)?"text-danger":"" }}" style="margin-bottom: 0;">{{ (isset($match->user1))?$match->user1["ps_id"]:"En attente" }}</p>
                                                    <p style="border-top: 1px solid black; margin-bottom: 0;" class="{{ (isset($match->user2) && $match->user2["id"] == $match->winner)?"text-danger":"" }}">{{ (isset($match->user2))?$match->user2["ps_id"]:"En attente" }}</p>
                                                @elseif($competition->console->id == 2)
                                                    <p class="rounded-top {{ (isset($match->user1) && $match->user1["id"] == $match->winner)?"text-danger":"" }}" style="margin-bottom: 0;">{{ (isset($match->user1))?$match->user1["xbox_id"]:"En attente" }}</p>
                                                    <p style="border-top: 1px solid black; margin-bottom: 0;" class="{{ (isset($match->user2) && $match->user2["id"] == $match->winner)?"text-danger":"" }}">{{ (isset($match->user2))?$match->user2["xbox_id"]:"En attente" }}</p>
                                                @else
                                                    <p class="rounded-top {{ (isset($match->user1) && $match->user1["id"] == $match->winner)?"text-danger":"" }}" style="margin-bottom: 0;">{{ (isset($match->user1))?$match->user1["warzone_id"]:"En attente" }}</p>
                                                    <p style="border-top: 1px solid black; margin-bottom: 0;" class="{{ (isset($match->user2) && $match->user2["id"] == $match->winner)?"text-danger":"" }}">{{ (isset($match->user2))?$match->user2["warzone_id"]:"En attente" }}</p>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        @else
                            <h4 class="text-white col-lg-12">Rendez vous sur le discord: <a style="color:red;" href="https://discord.gg/AjVj9SV5">https://discord.gg/3jkYcN46h9</a> pour suivre et participer à la compétition!!! ;-)</h4>
                        @endif
                    </div>
            </div>
        </div>
    </div>
    <script>
        @if(isset($myMatch->id) || (isset($adminMatchs) && count($adminMatchs) > 0))
            var x = document.getElementById('messages');

            function init(){
                loop();
            }
            function loop(cnt){
                //Toutes les 10 secondes
                setTimeout('loop();',10000);

                @if(isset($adminMatchs) && count($adminMatchs) > 0)
                    @foreach($adminMatchs as $match)
                        $.ajax({
                            type: "GET",
                            url: "{{ url("/messages/match/".$match->id) }}",
                            success:function(data){
                                $("#messages{{ $match->id }}").html(data);
                                x.scrollTop = x.scrollHeight;
                            }
                        });
                    @endforeach
                @else
                    $.ajax({
                        type: "GET",
                        url: "{{ url("/messages/match/".$myMatch->id) }}",
                        success:function(data){
                            $("#messages{{ $myMatch->id }}").html(data);
                            x.scrollTop = x.scrollHeight;
                        }
                    });
                @endif
            }

            init();
        @endif
    </script>
@endsection

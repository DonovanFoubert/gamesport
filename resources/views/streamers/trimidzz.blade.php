@extends('layouts.app')

@section('content')

    <div class="team_area team_bg_1 overlay2">
    <!-- Add a placeholder for the Twitch embed -->
        <div id="twitch-embed" class="features_area" style="text-align: center;"></div>

        <!-- Load the Twitch embed script -->
        <script src="https://player.twitch.tv/js/embed/v1.js"></script>

        <!-- Create a Twitch.Player object. This will render within the placeholder div -->
        <script type="text/javascript">
            new Twitch.Player("twitch-embed", {
                channel: "trimidzz",
                width: "60%",
                height: "500px",
                chat: "default"
            });
        </script>
    </div>

@endsection

@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2 text-center">
        <iframe width="50%" height="500px" src="https://www.youtube.com/embed/I6yUZu_H57g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
@endsection

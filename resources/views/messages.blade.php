@if(count($match->messages) === 0)
    <p class="col-12">Aucun message</p>
@else
    @foreach($match->messages as $message)
        <p class="col-12"><span class="{{ ($message->user_id == $message->match->user_id_1)? "text-danger" : "text-success" }}"><span style="font-size: 0.7em;">{{ $message->created_at }}</span> {{ $message->user->name }} : </span> {{ $message->message }} </p>
    @endforeach
@endif

@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container">
            @if(count($myCompetitions) > 0)
            <div class="row">
                <h2 class="col-12 text-white bold">Mes tournois {{ $game }}</h2>
                @foreach($myCompetitions as $competition)
                    <div class="col-lg-3 col-md-6 mt-5">
                        <div class="single_team">
                            <div class="team_thumb text-center rounded-top">
                                <img src="{{ asset($competition->game->image) }}" alt="" style="width:200px;">
                                <div style="width: 100%; margin-top: -25px;">
                                    <img class="{{ ($competition->live === 1)?"live":"" }}" src="{{ asset($competition->console->logo) }}" style="width: 100px; margin: 0 auto;"><br>
                                </div>
                                <div class="team_hover">
                                    <div class="hover_inner text-center">
                                        @if(isset($competition->cashprize) && !empty($competition->cashprize))<h2 class="text-white">Cashprize: {{ $competition->cashprize }}€</h2>
                                        @else<h2 class="text-white">Lot: {{ $competition->lot }}</h2>
                                        @endif
                                        <br><br>
                                        <h3 class="text-white">Inscription: {{ $competition->inscription }}€</h3>
                                        <br>
                                        <a class="boxed-btn2" href="{{ url("/competition/".$competition->id) }}">Détail</a>
                                    </div>
                                </div>
                            </div>
                            <div class="team_title text-center">
                                <p>{{ date("d/m/Y",strtotime($competition->date))." ".$competition->start." - ".$competition->end }}</p>
                                <p>Nombre de joueurs inscris: {{ count($competition->users) }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @endif
            @if(count($otherCompetitions) > 0)
            <div class="row">
                <h2 class="col-12 text-white bold">Les tournois {{ $game }} à venir</h2>
                @foreach($otherCompetitions as $competition)
                    <div class="col-lg-3 col-md-6 mt-5">
                        <div class="single_team">
                            <div class="team_thumb text-center rounded-top">
                                <img src="{{ asset($competition->game->image) }}" alt="" style="width:200px;">
                                <div style="width: 100%; margin-top: -25px;">
                                    <img src="{{ asset($competition->console->logo) }}" style="width: 100px; margin: 0 auto;"><br>
                                </div>
                                <div class="team_hover">
                                    <div class="hover_inner text-center">
                                        @if(isset($competition->cashprize) && !empty($competition->cashprize))<h2 class="text-white">Cashprize: {{ $competition->cashprize }}€</h2>
                                        @else<h2 class="text-white">Lot: {{ $competition->lot }}</h2>
                                        @endif
                                            <br><br>
                                        <h3 class="text-white">Inscription: {{ $competition->inscription }}€</h3>
                                        <br>
                                        <a class="boxed-btn2" href="{{ url("/competition/".$competition->id) }}">Détail</a>
                                    </div>
                                </div>
                            </div>
                            <div class="team_title text-center">
                                <p>{{ date("d/m/Y",strtotime($competition->date))." ".$competition->start." - ".$competition->end }}</p>
                                <p>Nombre de joueurs inscris: {{ count($competition->users) }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
                @endif
        </div>
    </div>

    <script>
        $(document).ready(function(){
            function FaireClignoterImage (){ $(".live").fadeOut(400).delay(0).fadeIn(400); }
            setInterval(FaireClignoterImage,1200);
        });
    </script>
@endsection

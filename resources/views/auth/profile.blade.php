@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12 col-md-6">
                    <h3 class="text-white">Mes informations</h3><br>
                    <form method="post" action="{{ url("/account/update") }}">
                        <div class="form-group row">
                            <label for="ps_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant PlayStation') }}</label>

                            <div class="col-md-6">
                                <input id="ps_id" type="text" class="form-control @error('ps_id') is-invalid @enderror" name="ps_id" value="{{ (isset($user->ps_id))?$user->ps_id:"" }}" autocomplete="ps_id" autofocus>

                                @error('ps_id')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="xbox_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant XBOX') }}</label>

                            <div class="col-md-6">
                                <input id="xbox_id" type="text" class="form-control @error('xbox_id') is-invalid @enderror" name="xbox_id" value="{{ (isset($user->xbox_id))?$user->xbox_id:"" }}" autocomplete="xbox_id">

                                @error('xbox_id')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="warzone_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant Warzone') }}</label>

                            <div class="col-md-6">
                                <input id="warzone_id" type="text" class="form-control @error('warzone_id') is-invalid @enderror" name="warzone_id" value="{{ (isset($user->warzone_id))?$user->warzone_id:"" }}" autocomplete="warzone_id">

                                @error('warzone_id')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="rocket_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant Rocket League') }}</label>

                            <div class="col-md-6">
                                <input id="rocket_id" type="text" class="form-control @error('rocket_id') is-invalid @enderror" name="rocket_id" value="{{ (isset($user->rocket_id))?$user->rocket_id:"" }}" autocomplete="rocket_id">

                                @error('rocket_id')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row col-xl-12">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <input type="submit" class="btn-danger m-auto">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card bg-dark">
                        <div class="card-header text-light">{{ __("S'enregistrer") }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf

                                <input type="hidden" name="prvUrl" value="{{ request('prvUrl') }}">

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right text-light">{{ __('Nom') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right text-light">{{ __('Adresse Email') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right text-light">{{ __('Mot de passe') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right text-light">{{ __('Confirmer mot de passe') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="ps_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant Playstation') }}</label>

                                    <div class="col-md-6">
                                        <input id="ps_id" type="text" class="form-control @error('ps_id') is-invalid @enderror" name="ps_id" value="{{ old('ps_id') }}" autocomplete="ps_id" autofocus>

                                        @error('ps_id')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="xbox_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant Xbox') }}</label>

                                    <div class="col-md-6">
                                        <input id="xbox_id" type="text" class="form-control @error('xbox_id') is-invalid @enderror" name="xbox_id" value="{{ old('xbox_id') }}" autocomplete="xbox_id" autofocus>

                                        @error('xbox_id')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="warzone_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant Warzone') }}</label>

                                    <div class="col-md-6">
                                        <input id="warzone_id" type="text" class="form-control @error('warzone_id') is-invalid @enderror" name="warzone_id" value="{{ old('warzone_id') }}" autocomplete="warzone_id" autofocus>

                                        @error('warzone_id')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="rocket_id" class="col-md-4 col-form-label text-md-right text-light">{{ __('Identifiant Rocket League') }}</label>

                                    <div class="col-md-6">
                                        <input id="rocket_id" type="text" class="form-control @error('rocket_id') is-invalid @enderror" name="rocket_id" value="{{ old('rocket_id') }}" autocomplete="rocket_id" autofocus>

                                        @error('rocket_id')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-danger">
                                            {{ __("S'enregistrer") }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="team_area team_bg_1 overlay2">
@endsection

@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Veuillez vérifier votre adresse mail') }}</div>

                        <div class="card-body">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('Un nouveau lien a été envoyé à votre adresse mail.') }}
                                </div>
                            @endif

                            {{ __("Avant de le lancer, merci de vérifier que le lien n'est pas parvenu à votre adresse mail.") }}
                            {{ __('Si vous ne souhaitez pas recevoir de mails') }},
                            <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                                @csrf
                                <button type="submit" class="btn btn-link p-0 m-0 align-baseline text-danger">{{ __('Cliquez ici pour en générer un nouveau') }}</button>.
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

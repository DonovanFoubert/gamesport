<!doctype html>
<html class="no-js" lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name', 'Gamesport') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

@yield('autoload')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("img/logo.png") }}">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/owl.carousel.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/magnific-popup.css") }}">
    <link href="{{ asset("/fontawesome/css/fontawesome.css") }}" rel="stylesheet">
    <link href="{{ asset("/fontawesome/css/brands.css") }}" rel="stylesheet">
    <link href="{{ asset("/fontawesome/css/solid.css") }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("css/themify-icons.css") }}">
    <link rel="stylesheet" href="{{ asset("css/gijgo.css") }}">
    <link rel="stylesheet" href="{{ asset("css/nice-select.css") }}">
    <link rel="stylesheet" href="{{ asset("css/flaticon.css") }}">
    <link rel="stylesheet" href="{{ asset("css/slicknav.css") }}">

    <link rel="stylesheet" href="{{ asset("css/style.css") }}">
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
<!-- <link rel="stylesheet" href="{{ asset("css/responsive.css") }}"> -->
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- header-start -->
<header>
    <div class="header-area">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid">
                <div class="header_bottom_border">
                    <div class="row align-items-center">
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo">
                                <a href="{{ url("/") }}" class="logo">
                                    <img src="{{ asset("img/logo_new_mini.png") }}" width="150px" alt=""><span class="versionning">version beta</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="nav-link {{ (Request::path() == '/') ? 'active' : '' }}" href="{{ url("/") }}">Accueil</a></li>
                                        <li><a class="nav-link {{ (Request::path() == 'competition') ? 'active' : '' }}" href="{{ url("/competition") }}">Compétitions</a></li>
                                        <li><a class="nav-link {{ (Request::path() == 'streamer') ? 'active' : '' }}" href="{{ url("/streamer") }}">Streamers</a></li>
                                        <li><a class="nav-link {{ (Request::path() == 'contact') ? 'active' : '' }}" href="{{ url("/contact") }}">Contact</a></li>
                                        <li><a class="nav-link" href=""> </a></li><li><a class="nav-link" href=""> </a></li>
                                        @guest
                                            <li class="right"> <a class="nav-link" href="{{ route('login') }}">{{ __('Connexion') }}</a></li>
                                            @if (Route::has('register'))
                                                <li class="right"><a class="nav-link" href="{{ route('register') }}">{{ __('Inscription') }}</a></li>
                                            @endif
                                        @else
                                            <li class="dropdown right"><a id="navbarDropdown" class="nav-link dropdown-toggle {{ (substr_count(Request::path(), 'account')) ? 'active' : '' }}" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" v-pre>{{ Auth::user()->name." ( ".Auth::user()->wallet." € )" }} <span class="caret"></span></a>

                                                <div class="dropdown-menu dropdown-menu" aria-labelledby="navbarDropdown">
                                                    <a class="dropdown-item" href="{{ url("/account/profile") }}">Mes informations</a>
                                                    <a class="dropdown-item" href="{{ url("/account/historic") }}">Mes transactions</a>
                                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Déconnexion') }}
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li>
                                        @endguest
                                    </ul>
                                    <!-- Authentication Links -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile_menu d-block d-lg-none"></div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->

@yield('content')

<!-- footer_start  -->
<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-md-6 col-lg-3 ">
                    <div class="footer_widget">
                        <div class="footer_logo">
                            <a href="#" class="logo">
                                <img src="{{ asset("img/logo_new_mini.png") }}" width="150px" alt="">
                            </a>
                        </div>
                        <p>GameSport - Prograticweb SRL</p>
                        <p>Chemin neuf, 5B - 7520 Templeuve - Belgique <br>
                            <a href="#">+32 475 71 11 43</a> - <a href="mailto:donovan@gamesport.online">donovan@gamesport.online</a>
                        </p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-youtube-play"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-3 col-md-4 col-lg-2 offset-xl-1">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Liens utiles
                        </h3>
                        <ul class="links">
                            <li><a target="_blank" href="https://gaming.prograticweb.be">Webshop</a></li>
                            <li><a href="{{ url("/competition") }}">Compétitions</a></li>
                            <li><a href="{{ url("/streamer") }}">Streamers</a></li>
                            <li><a href="{{ url("/contact") }}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-md-2 col-lg-2">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Aidez-nous! :-)
                        </h3>
                        <form action="https://www.paypal.com/donate" method="post" target="_top">
                            <input type="hidden" name="hosted_button_id" value="TJ8GWLYFSW8BA" />
                            <input style="border-radius: 5px; border: 2px solid white; width: 250px" type="image" src="https://gamesport.online/img/donation.jpg" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
                            <img alt="" border="0" src="https://www.paypal.com/fr_BE/i/scr/pixel.gif" width="1" height="1" />
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy; {{ date("Y")  }} All rights reserved | Powerder by <a href="https://prograticweb.be" target="_blank">Prograticweb</a> | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer_end  -->

<!-- JS here -->
<script src="{{ asset("js/vendor/modernizr-3.5.0.min.js") }}"></script>
<script src="{{ asset("js/vendor/jquery-1.12.4.min.js") }}"></script>
<script src="{{ asset("js/popper.min.js") }}"></script>
<script src="{{ asset("js/bootstrap.min.js") }}"></script>
<script src="{{ asset("js/owl.carousel.min.js") }}"></script>
<script src="{{ asset("js/isotope.pkgd.min.js") }}"></script>
<script src="{{ asset("js/ajax-form.js") }}"></script>
<script src="{{ asset("js/waypoints.min.js") }}"></script>
<script src="{{ asset("js/jquery.counterup.min.js") }}"></script>
<script src="{{ asset("js/imagesloaded.pkgd.min.js") }}"></script>
<script src="{{ asset("js/scrollIt.js") }}"></script>
<script src="{{ asset("js/jquery.scrollUp.min.js") }}"></script>
<script src="{{ asset("js/wow.min.js") }}"></script>
<script src="{{ asset("js/gijgo.min.js") }}"></script>
<script src="{{ asset("js/nice-select.min.js") }}"></script>
<script src="{{ asset("js/jquery.slicknav.min.js") }}"></script>
<script src="{{ asset("js/jquery.magnific-popup.min.js") }}"></script>
<script src="{{ asset("js/plugins.js") }}"></script>

<!--contact js-->
<script src="{{ asset("js/contact.js") }}"></script>
<script src="{{ asset("js/jquery.ajaxchimp.min.js") }}"></script>
<script src="{{ asset("js/jquery.form.js") }}"></script>
<script src="{{ asset("js/jquery.validate.min.js") }}"></script>
<script src="{{ asset("js/mail-script.js") }}"></script>

<script src="{{ asset("js/main.js") }}"></script>

</body>
</html>

@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container">
            <div class="row">
                @foreach($streamers as $streamer)
                    <div class="col-lg-3 col-md-3">
                        <div class="single_team">
                            <div class="team_thumb">
                                <img src="{{ asset($streamer->picture) }}" alt="">
                                <div class="team_hover">
                                    <div class="hover_inner text-center">
                                        <ul>
                                            <li><a href="{{ "http://www.twitch.com/".$streamer->twitch }}" target="_blank"><img src="{{ asset("/img/icon/play.png") }}" style="margin-top: -2px; margin-left: 2px;"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="team_title text-center">
                                <h3>{{ $streamer->pseudo }} - {{(int) ((time() - strtotime($streamer->birthday)) / 3600 / 24 / 365)}} ans</h3>
                                <p>{{ $streamer->function }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

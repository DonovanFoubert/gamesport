@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container contact-form" style="margin-top:100px">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
            @endif

            <form method="post" action="{{ route('contactus.store') }}">
                {{ csrf_field() }}
                <h4 class="text-white">Formulaire de contact</h4><br><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" name="name" class="form-control" placeholder="Votre nom *"  required />
                            @if ($errors->has('name'))
                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="Votre Email *"  required />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                            <input type="text" name="subject" class="form-control" placeholder="Sujet *"  />
                            @if ($errors->has('subject'))
                                <span class="help-block">
                                                    <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            <textarea name="message" class="form-control" placeholder="Votre Message *" style="width: 100%; height: 150px;" required></textarea>
                            @if ($errors->has('message'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                        </div>
                            @endif
                    </div>
                    <div class="form-group">
                        <input type="submit" name="btnSubmit" class="boxed-btn3" value="Envoyer le message" />

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

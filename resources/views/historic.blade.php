@extends('layouts.app')

@section('content')
    <div class="team_area team_bg_1 overlay2">
        <div class="container">
            <h2 class="text-white">Solde actuel: <span class="text-success">{{ auth()->user()->wallet }} €</span></h2><br>
            <h3 class="text-white col-xl-12">Historique de mes transactions</h3><br>
            <div class="row bg-dark rounded-lg text-center">
                <div class="col-xl-2 p-3 text-white font-weight-bold"></div>
                <div class="col-xl-2 p-3 text-white font-weight-bold">Date</div>
                <div class="col-xl-2 p-3 text-light font-weight-bold">Action</div>
                <div class="col-xl-2 p-3 text-light font-weight-bold">Montant</div>
                <div class="col-xl-1 p-3 text-white font-weight-bold">Solde</div>
                <div class="col-xl-2 p-3 text-white font-weight-bold"></div>
            </div><br>
            @foreach($historics as $historic)
                <div class="row bg-dark rounded-lg text-center">
                    <div class="col-xl-2 p-3 text-white font-weight-bold"></div>
                    <div class="col-xl-2 p-3 text-white font-weight-bold">{{ date("d-m-Y H:i:s", strtotime($historic->created_at)) }}</div>
                    <div class="col-xl-2 p-3 text-light font-weight-bold">{{ $historic->action }}</div>
                    <div class="col-xl-2 p-3 {{ ($historic->type == 'debit') ? 'text-success' : 'text-danger' }} font-weight-bold">{{ $historic->type." ".$historic->amount }} €</div>
                    <div class="col-xl-1 p-3 text-white font-weight-bold">{{ $historic->wallet }}€</div>
                    <div class="col-xl-2 p-3 text-white font-weight-bold"></div>
                </div><br>
            @endforeach
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
<!-- slider_area_start -->
<div class="slider_area">
    <div class="slider_active owl-carousel">
        <div class="single_slider  d-flex align-items-center slider_bg_1">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <span>Lancez vous dans la</span>
                            <h3>Compétition</h3>
                            <p>Avec nos tournois et championnats hebdomadaires</p>
                            <a href="{{ url("/competition") }}" class="boxed-btn3">Par ici</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_slider  d-flex align-items-center slider_bg_4 overlay">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <span>Suivez nos</span>
                            <h3>Streamers</h3>
                            <p>Qu'ils soient compétiteurs ou simples joueurs, fun assuré!</p>
                            <a href="{{ url("/streamer") }}" class="boxed-btn3">Voir la liste des streamers</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="single_slider  d-flex align-items-center slider_bg_2">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <span>Découvrez notre partenaire</span>
                            <h3>Webshop</h3>
                            <p>et faites vous plaisir!</p>
                            <a  target="_blank" href="https://gaming.prograticweb.be" class="boxed-btn3">Accéder au webshop</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</div>
<!-- slider_area_end -->

<!-- features_area_start  -->
<div class="features_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-73">
                    <h3>Nos jeux</h3>
                    <p>Liste assez courte pour le moment mais qui s'agrandira au plus vite! ;-)</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-6">
                <div class="single_feature text-center mb-73">
                    <div class="icon">
                        <a href="{{ url("/competition/game/1") }}"><img src="{{ asset("img/games/fifa-21.jpg") }}" width="150px" height="225px" alt=""></a>
                    </div>
                    <p><span class="text-success">Disponible</span></p>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single_feature text-center mb-73">
                    <div class="icon">
                        <a href="{{ url("/competition/game/4") }}"><img src="{{ asset("img/games/warzone.jpg") }}" width="150px" height="225px" alt=""></a>
                    </div>
                    <p><span class="text-success">Disponible</span></p>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single_feature text-center mb-73">
                    <div class="icon">
                        <a href="{{ url("/competition/game/5") }}"><img src="{{ asset("img/games/fortnite.jpg") }}" width="150px" height="225px" alt=""></a>
                    </div>
                    <p><span class="text-success">Disponible</span></p>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single_feature text-center mb-73">
                    <div class="icon">
                        <a href="{{ url("/competition/game/6") }}"><img src="{{ asset("img/games/rocket_league.jpg") }}" width="150px" height="225px" alt=""></a>
                    </div>
                    <p><span class="text-success">Disponible</span></p>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single_feature text-center mb-73">
                    <div class="icon">
                        <img src="{{ asset("img/games/rainbow.jpg") }}" width="150px" height="225px" alt="">
                    </div>
                    <p><span class="text-warning">Prochainement</span></p>
                </div>
            </div>
            <div class="col-lg-2 col-md-6">
                <div class="single_feature text-center mb-73">
                    <div class="icon">
                        <img src="{{ asset("img/games/gt-sport.jpg") }}" width="150px" height="225px" alt="">
                    </div>
                    <p><span class="text-danger">En développement</span></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- features_area_end  -->

<!-- team_area_start  -->
<div class="team_area team_bg_1 overlay2">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-73">
                    <h3>L'équipe</h3>
                    <p>composée d'e-sportifs, de streamers et d'administrateurs</p>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($streamers as $streamer)
                <div class="col-lg-3 col-md-3">
                    <div class="single_team">
                        <div class="team_thumb">
                            <img src="{{ asset($streamer->picture) }}" alt="">
                            <div class="team_hover">
                                <div class="hover_inner text-center">
                                    <ul>
                                        <li><a href="{{ "http://www.twitch.com/".$streamer->twitch }}" target="_blank"><img src="{{ asset("/img/icon/play.png") }}" style="margin-top: -2px; margin-left: 2px;"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="team_title text-center">
                            <h3>{{ $streamer->pseudo }} - {{(int) ((time() - strtotime($streamer->birthday)) / 3600 / 24 / 365)}} ans</h3>
                            <p>{{ $streamer->function }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- team_area_end  -->

@endsection

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'start', 'end', 'console_id', 'game_id', 'inscription', 'cashprize'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function console()
    {
        return $this->belongsTo('App\Console');
    }

    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function matchs()
    {
        return $this->hasMany('App\Match');
    }
}

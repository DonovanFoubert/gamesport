<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gamer extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pseudo', 'firstname', 'name', 'picture', 'function', 'birthday'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['firstname', 'name', 'birthday'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    protected $table = 'matchs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id_1', 'user_id_2', 'tournament_id', 'next_match', 'next_position', 'result1', 'result2', 'round', 'winner'
    ];

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function user1()
    {
        return $this->belongsTo('App\User', 'user_id_1');
    }

    public function user2()
    {
        return $this->belongsTo('App\User', 'user_id_2');
    }

    public function tournament()
    {
        return $this->belongsTo('App\Tournament');
    }

    public function winner()
    {
        return $this->belongsTo('App\User', 'winner');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    public $table = 'messages';

    public $fillable = ['message','match_id','user_id'];

    public function match()
    {
        return $this->belongsTo('App\Match');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    static public function check()
    {
        if (Auth::user() &&  Auth::user()->is_admin == 1) {
            return true;
        }

        return false;
    }
}

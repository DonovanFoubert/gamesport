<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit()
    {
        if(!Auth::check()) {
            return view('auth.login');
        }
        $user = Auth::user();
        return view('auth.profile', compact('user'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {
        if(!Auth::check()) {
            return view('auth.login');
        }
        $user = Auth::user();
        $competitions = $user->tournaments;

        if(empty($request["ps_id"])){
            foreach($competitions as $competition){
                if((int)$competition->console->id === 1){
                    return back()->with('warning', 'Vous ne pouvez pas supprimer votre Identifiant PlayStation, vous avez des tournois en cours ou terminés.');
                }
            }
        }

        if(empty($request["xbox_id"])){
            foreach($competitions as $competition){
                if((int)$competition->console->id === 2){
                    return back()->with('warning', 'Vous ne pouvez pas supprimer votre Identifiant XBOX, vous avez des tournois en cours ou terminés.');
                }
            }
        }

        if(empty($request["warzone_id"])){
            foreach($competitions as $competition){
                if((int)$competition->console->id === 3){
                    return back()->with('warning', 'Vous ne pouvez pas supprimer votre Identifiant Warzone, vous avez des tournois en cours ou terminés.');
                }
            }
        }

        if(empty($request["rocket_id"])){
            foreach($competitions as $competition){
                if((int)$competition->console->id === 4){
                    return back()->with('warning', 'Vous ne pouvez pas supprimer votre Identifiant Rocket League, vous avez des tournois en cours ou terminés.');
                }
            }
        }

        $user->ps_id = $request["ps_id"];
        $user->xbox_id = $request["xbox_id"];
        $user->warzone_id = $request["warzone_id"];
        $user->rocket_id = $request["rocket_id"];

        $user->save();
        return back()->with('success', 'Vos informations sont à jour');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest');
        $this->redirectTo = (isset($request["prvUrl"]))?$request["prvUrl"]:url('home');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'ps_id' => ['nullable', 'string', 'max:40', 'required_without_all:xbox_id,warzone_id,rocket_id'],
            'xbox_id' => ['nullable', 'string', 'max:40', 'required_without_all:ps_id,warzone_id,rocket_id'],
            'warzone_id' => ['nullable', 'string', 'max:40', 'required_without_all:ps_id,xbox_id,rocket_id'],
            'rocket_id' => ['nullable', 'string', 'max:40', 'required_without_all:ps_id,xbox_id,warzone_id'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'ps_id' => $data['ps_id'],
            'xbox_id' => $data['xbox_id'],
            'warzone_id' => $data['xbox_id'],
            'rocket_id' => $data['rocket_id'],
        ]);
    }
}

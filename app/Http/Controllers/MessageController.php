<?php

namespace App\Http\Controllers;

use App\Http\Middleware\IsAdmin;
use App\Repositories\MatchRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Message;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    protected $matchRepository;

    public function __construct(MatchRepository $matchRepository)
    {
        $this->matchRepository = $matchRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function messageSaveData(Request $request)
    {
        $request["user_id"] = Auth::user()->id;

        $this->validate($request, [
            'message' => 'required',
            'match_id' => 'required',
            'user_id' => 'required'
        ]);

        $match = $this->matchRepository->find($request["match_id"]);

        if(!isset($match->id)){
            return back()->with('warning', "Vous essayer de faire quelque chose d'innatendu... ;-)");
        }

        if($match->user_id_1 != Auth::user()->id && $match->user_id_2 != Auth::user()->id && !IsAdmin::check()){
            return back()->with('warning', "Vous ne participez pas à ce match... ;-)");
        }

        Message::create($request->all());

        return back()->with('success', 'Message envoyé!');
    }

    public function getByMatch($id)
    {
        $match = $this->matchRepository->find($id);

        return view('messages', compact('match'));
    }
}

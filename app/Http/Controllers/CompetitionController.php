<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Middleware\IsAdmin;
use App\Repositories\GameRepository;
use App\Repositories\MatchRepository;
use App\Repositories\TournamentUserRepository;
use App\Repositories\WalletHistoricRepository;
use App\WalletHistoric;
use App\User;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use App\Repositories\TournamentRepository;
use Illuminate\Support\Facades\Auth;

class CompetitionController extends Controller
{

    protected $tournamentRepository;
    protected $matchRepository;
    protected $gameRepository;

    protected $nbrPerPage = 4;

    public function __construct(TournamentRepository $tournamentRepository, MatchRepository $matchRepository, GameRepository $gameRepository)
    {
        $this->tournamentRepository = $tournamentRepository;
        $this->matchRepository = $matchRepository;
        $this->gameRepository = $gameRepository;
    }

    /**
     * Show the competition list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id = 0)
    {
        $game = "";

        if($id > 0){
            $competitions = $this->tournamentRepository->getByGameId($id);
            $game = $this->gameRepository->find($id)->title;
        } else {
            $competitions = $this->tournamentRepository->all();
        }

        $myCompetitions = $otherCompetitions = [];

        foreach($competitions as $competition){
            $suscribed = 0;

            if(Auth::check()) {
                foreach ($competition->users as $user) {

                    if ($user->id == Auth::user()->getAuthIdentifier()) {
                        $suscribed = 1;
                    }
                }
            }
            if($suscribed){
                $myCompetitions[] = $competition;
            } else {
                $otherCompetitions[] = $competition;
            }
        }

        return view('competition', compact('myCompetitions', 'otherCompetitions', 'game'));
    }

    /**
     * Show the competition list.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function launch()
    {
        $competitions = $this->tournamentRepository->toLauch();
        $previousMatchs = [null];
        $currentMatchs = [];
        $playersByMatch = 2;
        $arrayUsers = [];

        foreach($competitions as $competition){
            $round = 1;
            $nbSubscribes = count($competition->users);
            if($nbSubscribes > 1) {
                $users = $competition->users;

                foreach ($users as $user){
                    $arrayUsers[] = $user->id;
                    mail($user->email,"Le tournoi ".$competition->game->title." auquel vous êtes inscris est lancé","Merci de vous connecter sur https://www.gamesport.online pour contacter votre adversaire et enregistrer vos résultats.");
                }

                sort($arrayUsers);

                //Génération des matchs
                for ($cpt = 1; $cpt < $nbSubscribes; $cpt *= $playersByMatch) {
                    foreach ($previousMatchs as $previousMatch) {
                        if (isset($previousMatch)) {
                            $currentMatchs[] = $this->matchRepository->store(["tournament_id" => $competition->id, "next_match" => $previousMatch->id, "round" => $round, "next_position" => 2]);
                            $currentMatchs[] = $this->matchRepository->store(["tournament_id" => $competition->id, "next_match" => $previousMatch->id, "round" => $round, "next_position" => 1]);
                        } else {
                            $currentMatchs[] = $this->matchRepository->store(["tournament_id" => $competition->id, "next_match" => null, "round" => $round]);
                        }
                    }
                    $previousMatchs = $currentMatchs;
                    $currentMatchs = [];
                    $round *= 2;
                }

                //Répartition des joueurs
                for ($i = 1; $i <= 2; $i++){
                    foreach ($previousMatchs as $previousMatch) {
                        if(count($arrayUsers) == 0){
                            continue;
                        }
                        $key = rand(0, count($arrayUsers)-1);
                        if ($i === 1) {
                            $previousMatch->user_id_1 = $arrayUsers[$key];
                            $previousMatch->save();
                        } else {
                            $previousMatch->user_id_2 = $arrayUsers[$key];
                            $previousMatch->save();
                        }
                        unset($arrayUsers[$key]);
                        sort($arrayUsers);
                    }
                }

                //Définir les gagnants en cas de "Bye";
                foreach ($previousMatchs as $previousMatch) {
                    if(!isset($previousMatch->user_id_2)){
                        $previousMatch->winner = $previousMatch->user_id_1;
                        $previousMatch->user_id_2 = 0;
                        $previousMatch->save();

                        $nextMatch = $this->matchRepository->find($previousMatch->next_match);
                        if($previousMatch->next_position === 1){
                            $nextMatch->user_id_1 = $previousMatch->user_id_1;
                        } else {
                            $nextMatch->user_id_2 = $previousMatch->user_id_1;
                        }
                        $nextMatch->save();
                    }
                }
                $competition->live = 1;
            } else {
                $competition->live = 2;
            }
            $competition->save();
        }

        return true;
    }

    /**
     * Show the competition detail.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detail($id)
    {
        $competition = $this->tournamentRepository->find($id);
        $suscribed = 0;
        $rounds = [];
        $myMatch = null;
        $adminMatchs = [];

        if(Auth::check()) {
            foreach($competition->users as $user){
                if(Auth::user()->id == $user->id){
                    $suscribed = 1;
                }
            }
        }

        foreach($competition->matchs as $match){
            $rounds[$match->round][] = $match;

            if(Auth::check()) {
                if (($match->user_id_1 == Auth::user()->id || $match->user_id_2 == Auth::user()->id) && !isset($match->winner)) {
                    $myMatch = $match;
                }
            }
            if(IsAdmin::check()) {
                if (!isset($match->winner) && isset($match->user_id_1) && isset($match->user_id_2)) {
                    $adminMatchs[] = $match;
                }
            }
            krsort($rounds[$match->round]);
        }

        krsort($rounds);

        return view('competition_detail', compact('competition', 'suscribed', 'rounds', 'myMatch', 'adminMatchs'));
    }

    /**
     * Subsribe to competition.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function result($id, $result, WalletHistoricRepository $wallet)
    {
        if(!Auth::check()) {
            return view('auth.login');
        }

        $result = (int)$result;

        if($result !== 1 && $result !== 0){
            return back()->with('warning', 'Vous essayez quelque chose de tordu! ;-)');
        }

        $match = $this->matchRepository->find($id);

        if(isset($match) && ($match->user_id_1 == Auth::user()->id || $match->user_id_2 == Auth::user()->id)){
            if($match->user_id_1 == Auth::user()->id){
                $match->result_1 = $result;
                if(isset($match->result_2)){
                    if($match->result_2 != $match->result_1){
                        if($match->result_2 === 1){
                            $match->winner = $match->user_id_2;
                        } else {
                            $match->winner = $match->user_id_1;
                        }
                    } else{
                        mail('donovan@gamesport.online',"Résultats erronés!!!","Match $id / Utilisateur ".Auth::user()->id);
                        return back()->with('warning', 'Votre résultat et celui de votre adversaire ne correspondent pas! Veuillez utiliser le formulaire de contact pout le signaler!');
                    }
                }
            } elseif($match->user_id_2 == Auth::user()->id) {
                $match->result_2 = $result;
                if(isset($match->result_1)){
                    if($match->result_2 != $match->result_1){
                        if($match->result_2 === 1){
                            $match->winner = $match->user_id_2;
                        } else {
                            $match->winner = $match->user_id_1;
                        }
                    } else{
                        mail('donovan@gamesport.online',"Résultats erronés!!!","Match $id / Utilisateur ".Auth::user()->id);
                        return back()->with('warning', 'Votre résultat et celui de votre adversaire ne correspondent pas! Veuillez utiliser le formulaire de contact pout le signaler!');
                    }
                }
            }
        }

        $match->save();

        $this->generateNextMatch($match, $wallet);

        return back()->with('success', "Résultat entré pris en compte, les deux joueurs doivent le valider pour passer à l'étape suivante.");
    }

    /**
     * Subsribe to competition.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function inscription($id, WalletHistoricRepository $wallet)
    {
        if(!Auth::check()) {
            return view('auth.login');
        }

        $competition = $this->tournamentRepository->find($id);

        if($competition->live > 0){
            return back()->with('warning', "Il n'est plus possible de s'inscrire à cette compétition");
        }
        $suscribed = 1;

        $user = User::find(Auth::user()->id);
        $user->wallet -= $competition->inscription;

        if($user->wallet < 0){
            return back()->with('warning', 'Votre solde ne vous permet pas de vous inscrire...');
        }

        if($competition->console->id == 3) {
            if (empty($user->warzone_id)) {
                return back()->with('warning', "Vous n'avez pas entré d'Identifait Warzone. Rendez-vous dans la partie 'Mes informations' de votre profil pour l'enregistrer.");
            }
        } else if($competition->console->id == 4) {
            if (empty($user->rocket_id)) {
                return back()->with('warning', "Vous n'avez pas entré d'Identifait Rocket League. Rendez-vous dans la partie 'Mes informations' de votre profil pour l'enregistrer.");
            }
        } else if($competition->console->id == 2){
            if (empty($user->xbox_id)) {
                return back()->with('warning', "Vous n'avez pas entré d'Identifait XBOX. Rendez-vous dans la partie 'Mes informations' de votre profil pour l'enregistrer.");
            }
        } elseif($competition->console->id == 1){
            if (empty($user->ps_id)) {
                return back()->with('warning', "Vous n'avez pas entré d'Identifait PlayStation. Rendez-vous dans la partie 'Mes informations' de votre profil pour l'enregistrer.");
            }
        }

        $user->save();

        $competition->users()->attach(Auth::user());

        $inputs = ["user_id" => $user->id,"action" => "Inscription tournoi $id","type" => "credit","amount" => $competition->inscription,"wallet" => $user->wallet];
        $wallet->store($inputs);

        return back()->with('success', 'Merci de vous être inscris!');
    }

    /**
     * Unubsribe to competition.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function desinscription($id, WalletHistoricRepository $wallet)
    {
        if(!Auth::check()) {
            return view('auth.login');
        }

        $competition = $this->tournamentRepository->find($id);

        if($competition->live > 0){
            return back()->with('warning', "Il n'est plus possible de se se désinscrire de cette compétition");
        }

        $suscribed = 0;

        $user = User::find(Auth::user()->id);
        $user->wallet += $competition->inscription;
        $user->save();

        $competition->users()->detach(Auth::user());

        $inputs = ["user_id" => $user->id,"action" => "Annulation tournoi $id","type" => "debit","amount" => $competition->inscription,"wallet" => $user->wallet];
        $wallet->store($inputs);

        return back()->with('success', 'Vous êtes bien désinscris, dommage...');
    }

    /**
     * Force a winner of match.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function forceWinner($id, $winner, WalletHistoricRepository $wallet)
    {
        if(!Auth::check()) {
            return view('auth.login');
        }

        if(!IsAdmin::check()){
            return back()->with('warning', 'Vous ne pouvez pas effectuer cette action');
        }

        $match = $this->matchRepository->find($id);

        if($winner == 2){
            $winner = $match->user_id_2;
        } else {
            $winner = $match->user_id_1;
        }

        $match->winner = $winner;
        $match->save();

        $this->generateNextMatch($match, $wallet);

        return back()->with('success', 'Le vainqueur a bien été désigné');
    }

    private function generateNextMatch($match, $wallet){
        if(isset($match->next_match) && isset($match->winner)) {
            $nextMatch = $this->matchRepository->find($match->next_match);
            if($match->next_position === 1){
                $nextMatch->user_id_1 = $match->winner;
            } else {
                $nextMatch->user_id_2 = $match->winner;
            }
            $nextMatch->save();
        } else {
            if(isset($match->winner) && $match->winner === $match->user_id_1 && $match->tournament->live != 2){
                $match->tournament->live = 2;
                $match->tournament->save();

                $user = $match->user1;
                $user->wallet += (($match->tournament->cashprize*65)/100);
                $user->save();

                $inputs = ["user_id" => $user->id,"action" => "1er au tournoi ".$match->tournament->id,"type" => "debit","amount" => (($match->tournament->cashprize*65)/100),"wallet" => $user->wallet];
                $wallet->store($inputs);

                $user = $match->user2;
                $user->wallet += (($match->tournament->cashprize*35)/100);
                $user->save();

                $inputs = ["user_id" => $user->id,"action" => "2ème au tournoi ".$match->tournament->id,"type" => "debit","amount" => (($match->tournament->cashprize*35)/100),"wallet" => $user->wallet];
                $wallet->store($inputs);
            } elseif(isset($match->winner) && $match->winner === $match->user_id_2 && $match->tournament->live != 2) {
                $match->tournament->live = 2;
                $match->tournament->save();

                $user = $match->user2;
                $user->wallet += (($match->tournament->cashprize*65)/100);
                $user->save();

                $inputs = ["user_id" => $user->id,"action" => "1er au tournoi ".$match->tournament->id,"type" => "debit","amount" => (($match->tournament->cashprize*65)/100),"wallet" => $user->wallet];
                $wallet->store($inputs);

                $user = $match->user1;
                $user->wallet += (($match->tournament->cashprize*35)/100);
                $user->save();

                $inputs = ["user_id" => $user->id,"action" => "2ème au tournoi ".$match->tournament->id,"type" => "debit","amount" => (($match->tournament->cashprize*35)/100),"wallet" => $user->wallet];
                $wallet->store($inputs);
            }
        }
    }
}

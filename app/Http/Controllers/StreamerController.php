<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GamerRepository;

class StreamerController extends Controller
{

    protected $gamerRepository;

    protected $nbrPerPage = 4;

    public function __construct(GamerRepository $gamerRepository)
    {
        $this->gamerRepository = $gamerRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $streamers = $this->gamerRepository->all($this->nbrPerPage);

        return view('gamers', compact('streamers'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Repositories\GamerRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $gamerRepository;

    protected $nbrPerPage = 4;

    public function __construct(GamerRepository $gamerRepository)
    {
        $this->gamerRepository = $gamerRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $streamers = $this->gamerRepository->all();

        return view('index', compact('streamers'));
    }
}

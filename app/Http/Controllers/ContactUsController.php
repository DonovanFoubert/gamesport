<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ContactUS;

class ContactUSController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactUS()
    {
        return view('contact');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactSaveData(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'subject' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        ContactUS::create($request->all());

        $message = "Hello Donovan,"."
Nom  : ".$request->get('name')."
Email: ".$request->get('email')."
Message: ".$request->get('message');

        mail('donovan@gamesport.online',$request->get('subject'),$message);

        return back()->with('success', 'Merci de nous avoir contacté!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WalletHistoricRepository;
use Illuminate\Support\Facades\Auth;

class WalletHistoricController extends Controller
{

    protected $walletHistoricRepository;

    protected $nbrPerPage = 4;

    public function __construct(WalletHistoricRepository $walletHistoricRepository)
    {
        $this->walletHistoricRepository = $walletHistoricRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function historic()
    {
        if(!Auth::check()) {
            return view('auth.login');
        }

        $historics = $this->walletHistoricRepository->findByUserId(Auth::user()->id);

        return view('historic', compact('historics'));
    }
}

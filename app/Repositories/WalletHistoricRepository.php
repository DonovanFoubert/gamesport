<?php

namespace App\Repositories;

use App\WalletHistoric;

class WalletHistoricRepository
{

    protected $walletHistoric;

    public function __construct(WalletHistoric $walletHistoric)
    {
        $this->walletHistoric = $walletHistoric;
    }

    public function find($id){
        return $this->walletHistoric->find($id);
    }

    public function store($inputs)
    {
        $this->walletHistoric->create($inputs);
    }

    public function findByUserId($userId)
    {
        return $this->walletHistoric->whereUserId($userId)->get();
    }

    public function destroy($id)
    {
        //Attention utiliser userId et tournamentId pour retrouver le tournoi
        $this->walletHistoric->findOrFail($id)->delete();
    }

}

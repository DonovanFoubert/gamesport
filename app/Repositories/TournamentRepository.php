<?php

namespace App\Repositories;

use App\Tournament;

class TournamentRepository
{

    protected $tournament;

    public function __construct(Tournament $tournament)
    {
        $this->tournament = $tournament;
    }

    public function find($id){
        return $this->tournament->with('matchs')->find($id);
    }

    public function all(){
        return $this->tournament->with('users')->where('live','<',2)->orderBy('date', 'ASC')->orderBy('start', 'ASC')->get();
    }

    public function getByGameId($id){
        return $this->tournament->with('users')->where('live','<',2)->where('game_id','=', $id)->orderBy('date', 'ASC')->limit(12)->get();
    }

    public function toLauch(){
        return $this->tournament->with('users')->where('date','<=', date("Y-m-d"))
                                                        ->where('start','<=', date("H:i:s"))
                                                        ->where('live','=',0)
                                                        ->orderBy('date', 'ASC')->get();
    }

    public function getPaginate($n)
    {
        return $this->tournament->with('game', 'console', 'users')
            ->orderBy('tournaments.start', 'asc')
            ->where('date','>', date("Y-m-d"))
            ->paginate($n);
    }

    public function store($inputs)
    {
        $this->tournament->create($inputs);
    }

    public function destroy($id)
    {
        $this->tournament->findOrFail($id)->delete();
    }

}

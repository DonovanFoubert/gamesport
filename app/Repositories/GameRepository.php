<?php

namespace App\Repositories;

use App\Game;

class GameRepository
{

    protected $game;

    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    public function find($id){
        return $this->game->find($id);
    }

    public function store($inputs)
    {
        $this->game->create($inputs);
    }

    public function destroy($id)
    {
        $this->game->findOrFail($id)->delete();
    }

}

<?php

namespace App\Repositories;

use App\Gamer;

class GamerRepository
{

    protected $gamer;

    public function __construct(Gamer $gamer)
    {
        $this->gamer = $gamer;
    }

    public function getPaginate($n)
    {
        return $this->gamer
            ->orderBy('gamers.birthday', 'asc')
            ->paginate($n);
    }

    public function all()
    {
        return $this->gamer
            ->orderBy('gamers.ordre', 'asc')
            ->get();
    }

    public function store($inputs)
    {
        $this->gamer->create($inputs);
    }

    public function destroy($id)
    {
        $this->gamer->findOrFail($id)->delete();
    }

}

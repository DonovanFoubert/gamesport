<?php

namespace App\Repositories;

use App\Match;

class MatchRepository
{

    protected $match;

    public function __construct(Match $match)
    {
        $this->match = $match;
    }

    public function find($id){
        return $this->match->find($id);
    }

    public function store($inputs)
    {
        return $this->match->create($inputs);
    }

    public function update($data){
        return $this->match->update($data);
    }

    public function findByUserId($userId)
    {
        return $this->match->where('user_id_1', "=", $userId)->orWhere('user_id_2', "=", $userId)->get();
    }

    public function destroy($id)
    {
        //Attention utiliser userId et tournamentId pour retrouver le tournoi
        $this->match->findOrFail($id)->delete();
    }

}

<?php

namespace App\Repositories;

use App\Tournament;

class TournamentUserRepository
{

    protected $tournamentUser;

    public function __construct(TournamentUser $tournamentUser)
    {
        $this->tournamentUser = $tournamentUser;
    }

    public function find($id){
        return $this->tournamentUser->find($id);
    }

    public function store($inputs)
    {
        $this->tournamentUser->create($inputs);
    }

    public function destroy($id)
    {
        //Attention utiliser userId et tournamentId pour retrouver le tournoi
        $this->tournamentUser->findOrFail($id)->delete();
    }

}

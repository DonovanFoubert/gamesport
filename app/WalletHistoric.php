<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalletHistoric extends Model
{
    public $table = 'wallet_historic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'action', 'amount', 'wallet'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

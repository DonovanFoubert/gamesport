<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ps_id', 'xbox_id', 'warzone_id', 'rocket_id', 'wallet'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tournaments()
    {
        return $this->belongsToMany('App\Tournament');
    }

    public function messages()
    {
        return $this->belongsToMany('App\Message');
    }

    public function matchs()
    {
        return $this->belongsToMany('App\Match');
    }

    public function winners()
    {
        return $this->belongsToMany('App\Match');
    }

    public function walletHistorics()
    {
        return $this->belongsToMany('App\WalletHiqtoric');
    }
}
